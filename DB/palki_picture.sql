-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2017 at 07:07 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `event_skutz`
--

-- --------------------------------------------------------

--
-- Table structure for table `palki_picture`
--

CREATE TABLE IF NOT EXISTS `palki_picture` (
`id` int(11) NOT NULL,
  `palki_name` varchar(111) COLLATE ucs2_unicode_520_ci NOT NULL,
  `palki_pic` varchar(111) COLLATE ucs2_unicode_520_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE ucs2_unicode_520_ci NOT NULL DEFAULT 'No',
  `about` longtext COLLATE ucs2_unicode_520_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=ucs2 COLLATE=ucs2_unicode_520_ci;

--
-- Dumping data for table `palki_picture`
--

INSERT INTO `palki_picture` (`id`, `palki_name`, `palki_pic`, `soft_deleted`, `about`) VALUES
(1, 'simple palki', '1.jpg', 'No', 'it"s a simple palki for bridegroom.Palki or Palanquin bears the true resemblance of our age old tradition. It served as a luxury transport, mainly for the women which with time faded away with the influx of improved automobiles. To revive this lost culture, Mr. Subrata Biswas initiated his dream company Chaturdola Agency, in 1999, who offer the most conventional range of Doli or palki to recreate the age old essence in a contemporary way.  Our line of Doli are mostly hired for different wedding purposes, irrespective of all communities, immersions purposes and other social events. We offer our services in various cities of India like Kolkata, Ranchi, Guwahati, Bengalurru etc. All our Doli are designed in the most elegant and stylish manner using the premium quality materials, fabrics and designer accessories.  Backed by a team of sensible professionals, our proprietorship firm has carved a niche for itself in this unique sector and is running successfully in the City of Joy, yielding a reputed client base all over the nation. W e offer a variant line of palkis that includes wooden palki, iron palki, beth palki, Indian Wedding palki, Asian wedding palki etc. We are also offering comprehensive services for the other communities offering designer dolis, Indian bridal dolis, silver dolis etc. Our company also has established reliable associations with wedding bands, Ghori suppliers etc and we also organize musical events by arranging DJ services. We ensure our clients with an assortment of unique services, which our clients can avail in the most affordable and subsidized services.'),
(2, 'exclusive palki', '2.jpg', 'No', 'this is a sweet palki for a bride couple.we think this palki is really  exclusive for any bride'),
(3, 'gorgeous palki', '10.jpg', 'Yes', 'it'),
(4, 'comfortable palki', '7.jpg', 'No', 'it'),
(5, 'Pretty palki', '8.jpg', 'No', 'it'),
(6, 'Cute palki', '28.jpg', 'No', 'it"s a simple palki for bridegroom.cute'),
(7, 'simple palki', '25.jpg', 'No', 'it"s a simple palki for bridegroom.'),
(8, 'gorgeous palki', '23.png', 'No', 'it"s a gorgeouspalki for bridegroom.'),
(9, 'comfortable palki', '22.jpg', 'No', 'it"s a comfortable palki for bridegroom.'),
(10, 'Cute palki', '21.jpg', 'No', 'it"s a simple palki for bridegroom.cute'),
(11, 'Pretty palki', '19.jpg', 'No', 'it'),
(12, 'simple palki', '18.jpg', 'No', 'it"s a simple palki for bridegroom.'),
(13, 'simple palki', '16.jpg', 'No', 'it'),
(14, 'comfortable palki', '14.JPG', 'No', 'it"s a comfortable palki for bridegroom.'),
(15, 'gorgeous palki', '13.jpg', 'No', 'it"s a gorgeouspalki for bridegroom.'),
(16, 'sweet palki', '12.jpeg', 'No', 'it"s a simple palki for bridegroom.sweet '),
(17, 'Pretty palki', '9.jpg', 'No', 'it');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `palki_picture`
--
ALTER TABLE `palki_picture`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `palki_picture`
--
ALTER TABLE `palki_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
