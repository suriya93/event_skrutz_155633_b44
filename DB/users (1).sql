-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2017 at 06:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `event_skutz`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL,
  `fav_stage` varchar(111) DEFAULT NULL,
  `fav_entrance` varchar(111) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`, `fav_stage`, `fav_entrance`) VALUES
(17, 'Test', 'User', 'tushar.chowdhury@gmail.com', '202cb962ac59075b964b07152d234b70', '01711111111', 'Chittagong', 'Yes', '0', NULL),
(18, 'sdjf', 'lksdjf', 'tusharbd@gmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', '5235', 'dfgdg', 'Yes', '0', NULL),
(19, 'asfds', 'sdfgs', 'x@y.z', '202cb962ac59075b964b07152d234b70', '4545', 'sfsj', '4ae15d1c46f25be8db9d07061463c5f0', '0', NULL),
(26, '', '', '', '', '', '', '0be33819d840f70c37607259534982d8', '0', NULL),
(29, 'mknkn', 'nmm.nm', 'tasnin18@gmail.com', '202cb962ac59075b964b07152d234b70', 'ghfghf', 'hjknjk', 'Yes', '0', NULL),
(31, 'Sadia', 'Sultana', 'sadiasultana012@gmail.com', '202cb962ac59075b964b07152d234b70', '111', 'ctg', 'Yes', '2,8,2,5,', '1,2,3,');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `fav_stage` (`fav_stage`), ADD KEY `fav_stage_2` (`fav_stage`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
