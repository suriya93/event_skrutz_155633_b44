<?php
/**
 * Created by PhpStorm.
 * User: tasni
 * Date: 1/27/2017
 * Time: 11:19 AM
 */

namespace App\Model;
use PDO;
use PDOException;


class Database{
    public $conn;


    public $username="root";
    public $password="";

    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->DBH  = new PDO("mysql:host=localhost;dbname=event_skutz", $this->username, $this->password);

        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
}
