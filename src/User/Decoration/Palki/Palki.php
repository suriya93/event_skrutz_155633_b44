<?php


namespace App\User\Decoration\Palki;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO, PDOException;
class Palki extends DB
{

    private $id;
    private $email;


    public function setData($getData){

        if (array_key_exists('email', $getData)) {
            $this->email = $getData['email'];
        }

        if (array_key_exists('id', $getData)) {
            $this->id = $getData['id'];
        }


    }


    public function view(){
        $query="SELECT * FROM `users` WHERE `users`.`email` =:email";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $row=$result->fetch(PDO::FETCH_OBJ);
        return $row;
    }// end of view()



    public function addFavourite(){


        $oneData = $this->view();

        $strFavPalki = $oneData->fav_palki;

        $strPalkiIDs = $strFavPalki . $this->id .",";

        $arrData = array($strPalkiIDs);

        $query="UPDATE `event_skutz`.`users` SET `fav_palki=?  WHERE `users`.`email` ='$this->email'";

        $STH=$this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        return Utility::redirect($_SERVER['HTTP_REFERER']);

    }

    public function removeFavourite(){


        $oneData = $this->view();

        $strFavPalki = $oneData->fav_palki;

        $strRemovePalki = $this->id.",";

        $arr1=explode(",",$strFavPalki);

        $arr2=explode(",",$strRemovePalki);

        $arr=array_diff($arr1,$arr2);

        $strPalkiIDs=implode(",",$arr);

        $arrData = array($strPalkiIDs.",");

        $query="UPDATE `event_skutz`.`users` SET `fav_palki`=?  WHERE `users`.`email` ='$this->email'";

        $STH=$this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        return Utility::redirect($_SERVER['HTTP_REFERER']);




    }


}