<?php
/**
 * Created by PhpStorm.
 * User: NEXT
 * Date: 3/2/2017
 * Time: 11:09 AM
 */

namespace App\User\venue\REVIEW;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;
use PDOException;
class Comment extends DB
{
    private $id;
    private $name;
    private $email;
    private $comment;


    public function setData($postData)
    {

        if (array_key_exists('id', $postData)) {
            $this->id = $postData['id'];
        }
        if (array_key_exists('name', $postData)) {
            $this->name= $postData['name'];
        }

        if (array_key_exists('email', $postData)) {
            $this->email = $postData['email'];
        }
        if (array_key_exists('comment', $postData)) {
            $this->comment = $postData['comment'];
        }


    }

    public function COMstore(){

        $arrData = array($this->name,$this->email,$this->comment);
        $sql = "INSERT INTO `comment`( `name`,`email`,`comment`) VALUES(?,?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted :( ");

        Utility::redirect('entranceIndex.php');


    }



}