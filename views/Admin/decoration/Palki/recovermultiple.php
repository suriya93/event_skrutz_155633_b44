<?php
require_once("../../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\Admin\decoration\Palki\Palki;

if(isset($_POST['mark'])) {

    $objPalki = new Palki();
    $objPalki->recoverMultiple($_POST['mark']);

    Utility::redirect("index.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashed.php");

}