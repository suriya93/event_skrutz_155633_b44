<?php
require_once("../../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objEntrance = new \App\Admin\decoration\Entrance\Entrance();
$objEntrance->setData($_GET);
$oneData = $objEntrance->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Entrance Edit Form</title>


    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Available-Styles</a> </td>

    </div>



    <form class="form-group f" action="update.php" method="post" enctype="multipart/form-data">

        Please Enter Stage's Name:
        <br>
        <input class="form-control" type="text" name="entranceName" value="<?php echo $oneData->entrance_name ?>">
        <br>
        Enter Stage's Picture:
        <input type = "file" name="entrancePicture" accept=".png, .jpg, .jpeg" >
        <br>

        Enter Stage Details:
        <input type = "text" name="aboutEntrance" value="<?php echo $oneData->about ?>" >

        <img src='../../../../images/EntranceFiles/<?php echo $oneData->entrance_pic ?>' style="width:100px;height:100px;" />


        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


