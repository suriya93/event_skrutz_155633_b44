<?php
require_once("../../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\Admin\decoration\Entrance\Entrance;

if(isset($_POST['mark'])) {

    $objEntrance = new Entrance();
    $objEntrance->recoverMultiple($_POST['mark']);

    Utility::redirect("entranceIndex.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashed.php");

}