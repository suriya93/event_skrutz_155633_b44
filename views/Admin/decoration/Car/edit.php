<?php
require_once("../../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objCar = new \App\Admin\decoration\Car\Car();
$objCar->setData($_GET);
$oneData = $objCar->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Car Edit Form</title>


    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
            font-style: italic;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("../../../../images/CarFiles/c1.jpg");
        }
    </style>


</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Available-Car list</a> </td>

    </div>



    <form class="form-group f" action="update.php" method="post" enctype="multipart/form-data">

        <font color="blue"> Please Enter Car's Name:
        <br>
        <input class="form-control" type="text" name="carName" value="<?php echo $oneData->car_name ?>">
        <br>
        Enter Car's Picture:
        <input type = "file" name="carPicture" accept=".png, .jpg, .jpeg" >
        <br>

        Enter Car Details:
        <input type = "text" name="aboutCar" value="<?php echo $oneData->about ?>" >

        <img src='../../../../images/CarFiles/<?php echo $oneData->car_pic ?>' style="width:100px;height:100px;" />


        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">
        </font>
    </form>

</div>




<script src="../../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


