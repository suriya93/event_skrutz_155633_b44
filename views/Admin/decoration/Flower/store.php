<?php

require_once("../../../../vendor/autoload.php");

$objFlower = new \App\Admin\decoration\Flower\Flower();

$fileName = $_FILES["flowerPicture"]["name"];

$source = $_FILES["flowerPicture"]["tmp_name"];

$destination = "../../../../images/FlowerFiles/$fileName";

move_uploaded_file($source,$destination);

$_POST["flowerPicture"] = $fileName;

$objFlower->setData($_POST);

$objFlower->store();


?>