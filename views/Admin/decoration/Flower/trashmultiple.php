<?php
require_once("../../../../vendor/autoload.php");

use \App\Admin\decoration\Flower\Flower;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$objFlower= new Flower();


$objFlower->trashMultiple($_POST['mark']);
    Utility::redirect("trashed.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("index.php");
}