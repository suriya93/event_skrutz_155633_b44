<?php
require_once("../../../vendor/autoload.php");

$objVenue = new \App\Admin\venue\Location();
$objVenue->setData($_GET);
$oneData = $objVenue->view();

if(isset($_GET['YesButton'])) $objVenue->delete();

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture - Single Person Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Are you sure you want to delete the following record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Venue Name</th>
            <th>Venue Location</th>
            <th>Venue Capacity</th>
            <th>Venue Cost</th>
            <th>File Name</th>
            <th>Venue Picture</th>
        </tr>

        <?php

            echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->venue_name</td>
                       <td>$oneData->venue_location</td>
                        <td>$oneData->venue_capacity</td>
                         <td>$oneData->venue_cost</td>
                     <td>$oneData->venue_picture</td>
                     <td><img src='UploadedFiles/$oneData->venue_picture' style=\"width:64px;height:64px;\" /></td>

                  </tr>
              ";

        ?>

    </table>

<?php
   echo "
          <a href='delete.php?id=$oneData->id&YesButton=1' class='btn btn-danger'>Yes</a>

          <a href='flowerIndex.php' class='btn btn-success'>No</a>
        ";

?>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>