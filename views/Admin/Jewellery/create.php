<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jewellery Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



    <style>


        td{
            border: 0px;
        }

        table{
            border: 10px;
            font-style: italic;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("../../../images/JewelleryFiles/b.jpg");
        }
    </style>



</head>
<body>


<div class="container">

    <div class="navbar">

        <td><a href='index.php' class="btn-group-lg bg-primary ">Available-Jewellery</a> </td>

    </div>



    <form class="form-group f" action = "store.php" method = "post" enctype="multipart/form-data">
        <font color="#ffd700"> Please Enter Jewellery Name:
        <br>
        <input class="form-control" type="text" name="jewelleryName">
        <br>
        Enter Jewellery's Picture:
        <input type = "file" name="jewelleryPicture" accept=".png, .jpg, .jpeg" >
        <br>
        Enter Jewellery Details:
        <input type = "text" name="aboutJewellery" >
        <input type="submit">
        <br>
        </font>
    </form>


    <script src="../../../resource/bootstrap/js/jquery.js"></script>

    <script>
        jQuery(function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        })
    </script>


</body>
</html>