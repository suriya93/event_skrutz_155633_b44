<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objJewellery= new \App\Admin\Jewellery\Jewellery();
$objJewellery->setData($_GET);
$oneData = $objJewellery->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jewellery Edit Form</title>


    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
            font-style: italic;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("../../../images/JewelleryFiles/joss2.jpg");
        }
    </style>


</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Available-Jewelleryes list</a> </td>

    </div>



    <form class="form-group f" action="update.php" method="post" enctype="multipart/form-data">

        Please Enter Jewellery's Name:
        <br>
        <input class="form-control" type="text" name="jewelleryName" value="<?php echo $oneData->jewellery_name ?>">
        <br>
        Enter Jewellery's Picture:
        <input type = "file" name="jewelleryPicture" accept=".png, .jpg, .jpeg" >
        <br>

        Enter Jewellery Details:
        <input type = "text" name="aboutJewellery" value="<?php echo $oneData->about ?>" >

        <img src='../../../../images/JewelleryFiles/<?php echo $oneData->jewellery_pic ?>' style="width:100px;height:100px;" />


        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


