<?php
require_once("../../../vendor/autoload.php");
$objJewellery = new \App\Admin\Jewellery\Jewellery();


if($_FILES["jewelleryPicture"]["name"]=="")
{

    $objJewellery->setData($_POST);
    $oneData =  $objJewellery->view();
    $fileName = $oneData->jewellery_pic;

}
else{
    $fileName = $_FILES["jewelleryPicture"]["name"];

    $source = $_FILES["jewelleryPicture"]["tmp_name"];
    $destination = "../../../images/JewelleryFiles/$fileName";
    move_uploaded_file($source,$destination);

}

$_POST["jewelleryPicture"] = $fileName;
$objJewellery->setData($_POST);

$objJewellery->update();



?>