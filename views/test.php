<?php

$arr1 = array('rabbit','cat','dog');

$arr2 = array('cat','dog','bird');

$diff1 = array_diff($arr1, $arr2);
$diff2 = array_diff($arr2, $arr1);
print_r( array_merge($diff1, $diff2) );

echo "<br>";

$old_article = "1,2,3";
$new_article = "1,"; /* Let's say that someone pasted a new article to html form */

$diff = chop($old_article, $new_article);
if (is_string($diff)) {
    echo "Differences between two articles:\n";
    echo $diff;
}

echo "<br>";

$str1= "1,2,3";

$str2="2,";

$arr1= explode(",",$str1);

$arr2= explode(",",$str2);

$arr = array_diff($arr1,$arr2);

$str= implode(",",$arr);

echo $str;



?>