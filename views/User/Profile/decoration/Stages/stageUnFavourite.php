<?php

require_once("../../../../../vendor/autoload.php");

use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}

$_GET['email']=$_SESSION['email'];

$objStage = new \App\User\Decoration\Stages\FavouriteStage();

$objStage->setData($_GET);

$objStage->removeFavourite();

?>