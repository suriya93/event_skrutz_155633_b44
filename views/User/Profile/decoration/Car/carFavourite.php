<?php

require_once("../../../../../vendor/autoload.php");

use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}

$_GET['email']=$_SESSION['email'];

$objCar = new \App\User\Decoration\Car\FavouriteCar();

$objCar->setData($_GET);

$objCar->addFavourite();

?>