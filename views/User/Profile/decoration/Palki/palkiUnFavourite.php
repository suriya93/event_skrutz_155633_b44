<?php

require_once("../../../../../vendor/autoload.php");

use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}

$_GET['email']=$_SESSION['email'];

$objPalki = new \App\User\Decoration\Palki\FavouritePalki();

$objPalki->setData($_GET);

$objPalki->removeFavourite();

?>