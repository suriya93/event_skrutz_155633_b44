<?php
require_once("../../../../../vendor/autoload.php");
$serial=0;



$objEntrance = new \App\Admin\decoration\Entrance\Entrance();

$someData = $objEntrance->getFavEntrances();

use App\Message\Message;
use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();

$_POST['email']=$_SESSION['email'];



if(isset($_SESSION['mark']))  unset($_SESSION['mark']);


################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $objEntrance->search($_REQUEST);
$availableKeywords=$objEntrance->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################




################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objEntrance->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################



?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Entrance - Favourites</title>
    <link rel="stylesheet" href="../../../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../../resource/bootstrap/css/bootstrap-theme.min.css">

    <script src="../../../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
            font-style: italic;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("bg2.jpg");
        }

    </style>


    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../../../resource/bootstrap/css/jquery-ui.css">
    <script src="../../../../../resource/bootstrap/js/jquery.js"></script>
    <script src="../../../../../resource/bootstrap/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body>


<div class="container">

    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $msg </div> </div>"; ?>

    <!-- required for search, block 4 of 5 start -->

    <div style="margin-left: 70%">
        <form id="searchForm" action="entranceIndex.php" method="get" style="margin-top: 5px; margin-bottom: 10px ">
            <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" >
            <input type="checkbox"  name="byName"   checked  >By Name
            <input type="checkbox"  name="byFileName" checked >By File
            <input hidden type="submit" class="btn-primary" value="search">
        </form>
    </div>

    <!-- required for search, block 4 of 5 end -->


    <form action="" method="post" id="multiple">


        <div class="navbar"?>

            <a href='entranceIndex.php' class='btn btn-info'>All Entrance</a>

        </div>




        <h1 style="text-align: center" >Entrance - My Favourites (<?php echo count($someData) ?>)</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>




            <th>Name</th>

            <th>Stage Picture</th>
            <th>Action Buttons</th>
        </tr>

        <?php
       // $serial= 1;
        foreach($someData as $oneData){

            if($serial%2) $bgColor = "#FFCCFF";
            else $bgColor = "#fbc3b0";

            echo " <tr  style='background-color: $bgColor'>




                     <td>$oneData->entrance_name</td>


                    <td style='padding-left: 4%' ><img src='../../../../../images/EntranceFiles/$oneData->entrance_pic' alt='stage_pic' width='500px' height='300px'></td>

                     <td>
                     <a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>

                     <a href='email.php?id=$oneData->id' class='btn btn-success'>Get Details via email</a>

                     <a href='entranceUnFavourite.php?id=$oneData->id' class='btn btn-danger'>Remove From Favourites</a>
                     </td>
                  </tr>
              ";
            $serial++;
        }
        ?>

    </table>

    </form>



</div>

<script src="../../../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeIn(500);
        $('#message').fadeOut (500);
        $('#message').fadeIn (500);
        $('#message').delay (2500);
        $('#message').fadeOut (2000);
    })

    $('#delete').on('click',function(){
        document.forms[1].action="deletemultiple.php";
        $('#multiple').submit();
    });



    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });







</script>







<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->





</body>
</html>