<?php

require_once("../../../../../vendor/autoload.php");

use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}

$_GET['email']=$_SESSION['email'];

$objEntrance = new \App\User\Decoration\Entrance\FavouriteEntrance();

$objEntrance->setData($_GET);

$objEntrance->addFavourite();

?>