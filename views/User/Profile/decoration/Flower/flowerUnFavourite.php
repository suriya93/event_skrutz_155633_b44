<?php

require_once("../../../../../vendor/autoload.php");

use App\Utility\Utility;

if(!isset($_SESSION)){
    session_start();
}

$_GET['email']=$_SESSION['email'];

$objFlower = new \App\User\Decoration\Flower\FavouriteFlower();

$objFlower->setData($_GET);

$objFlower->removeFavourite();

?>