<?php
require_once("../../../vendor/autoload.php");

$objStages = new \App\Admin\decoration\Stages\Stages();
$objStages->setData($_GET);
$oneData = $objStages->view();


?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Stages - Single Stage Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("bg4.jpg");
        }

    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Stages - Single Stage Information</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>

            <th>Name</th>
            <th>Stage Picture</th>
            <th>Action Buttons</th>
        </tr>

        <?php

            echo "

                  <tr >

                     <td>$oneData->stage_name</td>
                     <td style='padding-left: 3%'><img src='../../../images/StageFiles/$oneData->stage_pic'height='500px' width='600px'  /><br><br>
                     <button type='button' class='btn btn-info' data-toggle='collapse' data-target='#demo'>Show/Hide Details</button>
                        <div id='demo' class='collapse'>
                         $oneData->about
                     </td>


                     <td><a href='flowerIndex.php' class='btn btn-info'>Back</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>