<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->prepare($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('User/Profile/signin.php');
    return;
}
?>


<!DOCTYPE html>
<html>
<head>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/css/style.css">
</head>

<body>

<div class="container">

    <table align="center">
        <tr>
            <td height="100" >

                <div id="message" >

                    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                        echo "&nbsp;".Message::message();
                    }
                    Message::message(NULL);

                    ?>
                </div>

            </td>
        </tr>
    </table>


    <header class="tab-content">
        <h1>Hello <?php echo "$singleUser->first_name $singleUser->last_name"?>! </h1>
    </header>

    <nav>
        <ul>
            <li> <a href= "../../User/Authentication/logout.php" > LOGOUT </a></li>
        </ul>
    </nav>

    <article class="alert-danger">
        <h2>Welcome to the SKUTZ Registered Client Area... </h2><br>
        <p>Allow us to make your special days memorable. We are devoted to make your memories sweeter...</p>
        <p>Your wish....our command!!</p>
        <a href="decoration/Stages/stagesIndex.php" class='btn btn-success'>Stages</a>
        <a href="decoration/Entrance/entranceIndex.php" class='btn btn-success'>Entrance</a>
        <a href="catering/cateringIndex.html" class='btn btn-success'>Catering</a>



    </article>

    <footer class="alert-success">Copyright © test-example.com</footer>

</div>


<!-- Javascript -->
<script src="../../../resource/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/js/placeholder.js"></script>
<![endif]-->

</body>

<script>
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");
</script>

</html>
