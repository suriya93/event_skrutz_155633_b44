<?php
require_once("../../../vendor/autoload.php");

$objJewellery = new \App\Admin\Jewellery\Jewellery();
$objJewellery->setData($_GET);
$oneData = $objJewellery->view();


?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Jewellery - Single Jewellery Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }

        body{
            background-image: url("../../../images/JewelleryFiles/vv.gif");
        }

    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center" ;">Jewellery - Single Jewellery Information</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Name</th>
            <th>File Name</th>
            <th>Jewellery Picture</th>
            <th>Action Buttons</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->jewellery_name</td>
                     <td>$oneData->jewellery_pic</td>
                     <td style='padding-left: 3%'><img src='../../../images/JewelleryFiles/$oneData->jewellery_pic' height='500px' width='600px' /><br><br>
                   <button type='button' class='btn-group-lg' data-toggle='collapse' data-target='#demo'>  <font color='#dc143c'>Show/Hide Details</font> </button>
                        <div id='demo' class='collapse'>
                         $oneData->about
                     </td>


                     <td><a href='index.php' class='btn-group-lg bg-primary'>Back</a> </td>
                  </tr>
              ";

        ?>

    </table>

</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>

</body>
</html>